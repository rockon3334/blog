@extends('layouts.app')

@section('content')

    @if(Session::has('success'))
        <p class="alert alert-success">{{ Session::get('success') }}</p>
    @endif

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Categories
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>
                    Image
                </th>
                <th>
                    Title
                </th>
                <th>
                    Editing
                </th>
                <th>
                    Delete
                </th>
                </thead>

                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>
                            <img src="{{ $post->featured }}" alt="{{ $post->title }}" height="50px" width="50px">
                        </td>
                        <td>
                            {{ $post->title }}
                        </td>
                        <td>
                            <a class="btn btn-success btn-sm" href="{{ route('category.edit', ['id' =>$post->id]) }}">
                                Edit
                            </a>
                        </td>
                        <td>
                            <a class="btn-danger btn-sm" href="{{ route('category.delete', ['id' =>$post->id]) }}">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection